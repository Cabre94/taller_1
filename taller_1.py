#==============================================================================
# Taller 1 de Introduccion a la Computacion (B) 2do Cuatrimestre 2017
# Facundo Martin Cabrera 397/14 carrera Lic. en Cs. Fisicas
#==============================================================================

#==============================================================================
# Definimos una funcion auxiliar para determinar si un numero es primo
#==============================================================================

def esPrimo(n):                 #Defino una funcion para ver si n es primo o no
    c=0                         # Defino un contador para saber si hay otro di-
                                # visor de n ademas de 1 y n
    i=3                         # Definimos un contador i para verificar si n 
                                # es divisible por i, comenzamos desde el 3 ya
                                # el 1 siempre es divisor y el 2 lo vamos a
                                # analizar por separado
    if n < 2 or n % 1 != 0:     #Si n < 2 (negativo, uno o cero) o no es entero
                                # pedimos que sea False
        return False
    else:
        if n % 2 == 0 and n != 2:   # Analizamos si n es par 2 y excluimos al 2
                                # ya que este si es par
            c+=1
        while c==0 and i <= (n**0.5):   # Ejecuta el while mientras no haya 
                                #otro divisor ademas de 1 y n. Sabemos que si 
                                # no hay nigun divisor antes la raiz cuadrada
                                # de n, entonces solo tiene dos divisores
            if n % i == 0:
                c+=1            #Si i es divisor, ya se que n tendra al menos 
                                #3 divisores y no es primo, entonces le sumo 1
                                #a c para que salga del ciclo
            i+=2
        return c==0             #Solo dara True si n no tiene ningun divisor
                                #entre 2 y n-1, es decir, si es primo

#==============================================================================
# Ejercicio 1
# Defino la funcion Dybala que realice la sumatoria de i = 0 hasta n de los
# terminos con la forma (2*1^(i+1))/(2*i-1)
#==============================================================================

def Dybala(n):
    suma=0
    for i in range(1,n+1):      #Para n<1 simplemente el for no hace nada y
                                #la suma da cero
        suma += (((-1)**(i+1))*2)/(2*i-1)    #Algun parentesis es innecesario
                                #para que haga el calculo bien pero los puse
                                #para que quede mas claro
    return suma

#==============================================================================
# Ejercicio 2
# Defino la funcion Suarez(n) que devuelva el n-esimo primo de la forma (2^m)-1 
#==============================================================================

def Suarez(n):                  
    suma=0                      #Nos definimos el valor inicial de una varia-
                                #ble suma que representara la cantidad de pri-
                                #mos de la forma (2^m)-1 a medida que vamos
                                #tomando numeros mas grandes. Lo que queremos 
                                #de esta funcion es el valor del primo cuando
                                #suma=n
    i=0
    while n != suma:
        i += 1
        if  esPrimo(2**i-1):    #Si 2**i-1 es primo agregamos 1 a suma
            suma += 1
    return 2**i-1               #Cuando n = suma detenemos el while y pedimos 
                                #que nos devuelva el valor de i - 1 ya que 
                                #le sumo 1 al obtener n=suma en el ultimo ci-
                                #clo
